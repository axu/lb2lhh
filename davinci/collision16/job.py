"""
option file to submit ganga jobs
"""

import os
import sys

def submit(job_name,davinci_version,local_path,option_file,nperjob,data,useQuery=True,flag="OK"):
    j = Job()
    j.name = job_name
    
    # Prepare davinci
    cmt_path = '/afs/cern.ch/user/a/axu/cmtuser/'
    davinci_path = os.path.join(cmt_path,'DaVinciDev_'+davinci_version)
    if os.path.isdir(davinci_path):
        myApp = GaudiExec()
        myApp.directory = davinci_path
    else:
        myApp = prepareGaudiExec('DaVinci', davinci_version, myPath=cmt_path)
    myApp.platform = 'x86_64-slc6-gcc62-opt'
    #myApp.platform = 'x86_64-slc6-gcc7-opt'
    j.application = myApp
    
    # Data
    if useQuery:
        ds = BKQuery(data, dqflag = [flag]).getDataset()
        j.inputdata = ds
        log = open('inputdata_{0}.txt'.format(job_name),'w')
        sys.stdout = log
        print(ds)
        sys.stdout = sys.__stdout__
    else:
        j.application.readInputData(os.path.join(local_path,data))
    j.splitter = SplitByFiles( filesPerJob = nperjob )
    # Options
    j.application.options = [os.path.join(local_path,option_file)]
    # Output: the name should be the same as in the option file
    j.outputfiles = [ DiracFile('Tuple.root') ]

    j.backend = Dirac()
    j.submit()


submit(
     'Lb2Lhh_collision18_md'
    ,'v44r8'
    ,'/afs/cern.ch/user/a/axu/workdir/private/Lb2Lhh/davinci/collision16'
    ,'Lb2Lhh_collision18.py'
    ,50
    ,'/LHCb/Collision18/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco18/Stripping34/90000000/BHADRON.MDST'
)
submit(
     'Lb2Lhh_collision18_mu'
    ,'v44r8'
    ,'/afs/cern.ch/user/a/axu/workdir/private/Lb2Lhh/davinci/collision16'
    ,'Lb2Lhh_collision18.py'
    ,50
    ,'/LHCb/Collision18/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco18/Stripping34/90000000/BHADRON.MDST'
)

"""
submit(
     'Lb2Lhh_collision17_md'
    ,'v44r8'
    ,'/afs/cern.ch/user/a/axu/workdir/private/Lb2Lhh/davinci/collision16'
    ,'Lb2Lhh_collision17.py'
    ,50
    ,'/LHCb/Collision17/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco17/Stripping29r2/90000000/BHADRON.MDST'
)
submit(
     'Lb2Lhh_collision17_mu'
    ,'v44r8'
    ,'/afs/cern.ch/user/a/axu/workdir/private/Lb2Lhh/davinci/collision16'
    ,'Lb2Lhh_collision17.py'
    ,50
    ,'/LHCb/Collision17/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco17/Stripping29r2/90000000/BHADRON.MDST'
)

submit(
     'Lb2Lhh_collision16_md'
    ,'v44r8'
    ,'/afs/cern.ch/user/a/axu/workdir/private/Lb2Lhh/davinci/collision16'
    ,'Lb2Lhh_collision16.py'
    ,50
    ,'/LHCb/Collision16/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco16/Stripping28r1/90000000/BHADRON.MDST'
)
submit(
     'Lb2Lhh_collision16_mu'
    ,'v44r8'
    ,'/afs/cern.ch/user/a/axu/workdir/private/Lb2Lhh/davinci/collision16'
    ,'Lb2Lhh_collision16.py'
    ,50
    ,'/LHCb/Collision16/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco16/Stripping28r1/90000000/BHADRON.MDST'
)
"""
