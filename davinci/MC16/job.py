"""
option file to submit ganga jobs
"""

import os
import sys

def submit(job_name,davinci_version,local_path,option_file,nperjob,data,useQuery=True,flag="OK"):
    j = Job()
    j.name = job_name
    
    # Prepare davinci
    cmt_path = '/afs/cern.ch/user/a/axu/cmtuser/'
    davinci_path = os.path.join(cmt_path,'DaVinciDev_'+davinci_version)
    if os.path.isdir(davinci_path):
        myApp = GaudiExec()
        myApp.directory = davinci_path
    else:
        myApp = prepareGaudiExec('DaVinci', davinci_version, myPath=cmt_path)
    myApp.platform = 'x86_64-slc6-gcc62-opt'
    #myApp.platform = 'x86_64-slc6-gcc7-opt'
    j.application = myApp
    
    # Data
    if useQuery:
        ds = BKQuery(data, dqflag = [flag]).getDataset()
        j.inputdata = ds
        log = open('inputdata_{0}.txt'.format(job_name),'w')
        sys.stdout = log
        print(ds)
        sys.stdout = sys.__stdout__
    else:
        j.application.readInputData(os.path.join(local_path,data))
    j.splitter = SplitByFiles( filesPerJob = nperjob )
    # Options
    j.application.options = [os.path.join(local_path,option_file)]
    # Output: the name should be the same as in the option file
    j.outputfiles = [ DiracFile('Tuple.root') ]

    j.backend = Dirac()
    j.submit()

submit(
     'Lb2LKK_MC16_md'
    ,'v44r8'
    ,'/afs/cern.ch/user/a/axu/workdir/private/Lb2Lhh/davinci/MC16'
    ,'Lb2LKK_MC16_md.py'
    ,20
    ,'/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged/15104132/ALLSTREAMS.DST'
)

submit(
     'Lb2LKK_MC16_mu'
    ,'v44r8'
    ,'/afs/cern.ch/user/a/axu/workdir/private/Lb2Lhh/davinci/MC16'
    ,'Lb2LKK_MC16_mu.py'
    ,20
    ,'/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged/15104132/ALLSTREAMS.DST'
)

"""
submit(
     'Lb2LKPi_MC16_md'
    ,'v44r8'
    ,'/afs/cern.ch/user/a/axu/workdir/private/Lb2Lhh/davinci/MC16'
    ,'Lb2LKPi_MC16_md.py'
    ,20
    ,'/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged/15304111/ALLSTREAMS.DST'
)

submit(
     'Lb2LKPi_MC16_mu'
    ,'v44r8'
    ,'/afs/cern.ch/user/a/axu/workdir/private/Lb2Lhh/davinci/MC16'
    ,'Lb2LKPi_MC16_mu.py'
    ,20
    ,'/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged/15304111/ALLSTREAMS.DST'
)
"""
