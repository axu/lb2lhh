"""
Process stripping lines for Lb -> L h h decays
Signal modes:
    * StrippingLb2V0hhDDLine
    * StrippingLb2V0hhLDLine
    * StrippingLb2V0hhLLLine
Same-sign modes:
    * StrippingLb2V0hhDDSSLine
    * StrippingLb2V0hhLDSSLine
    * StrippingLb2V0hhLLSSLine
"""

from os import environ
from GaudiKernel.SystemOfUnits import *
from Gaudi.Configuration import *
from Configurables import (
        GaudiSequencer,
        CombineParticles,
        FilterDesktop,
        DecayTreeTuple,
        MCDecayTreeTuple,
        EventTuple,
        TupleToolTrigger,
        TupleToolTISTOS,
        TupleToolPrimaries,
        TupleToolDecay,
        TupleToolVtxIsoln,
        TupleToolPid,
        TupleToolRecoStats,
        TupleToolVeto,
        TupleToolDecayTreeFitter,
        TupleToolMCTruth,
        BackgroundCategory,
        TupleToolMCBackgroundInfo,
        EventCountHisto,
        LoKi__Hybrid__TupleTool,
        LoKi__Hybrid__EvtTupleTool
)
from DecayTreeTuple.Configuration import *

year = '2016'
stream = 'Bhadron'
lines = {
        'DD' : 'Lb2V0hhDDLine',
        'LD' : 'Lb2V0hhLDLine',
        'LL' : 'Lb2V0hhLLLine'
}
decays = '[Lambda_b0 -> ^pi+ ^pi- ^(Lambda0 -> ^p+ ^pi-)]CC'
branches = {
        'Lb' : '^([Lambda_b0 -> pi+ pi- (Lambda0 -> p+ pi-)]CC)',
        'L0' : '[Lambda_b0 -> pi+ pi- ^(Lambda0 -> p+ pi-)]CC',
        'hp' : '[Lambda_b0 -> ^pi+ pi- (Lambda0 -> p+ pi-)]CC',
        'hm' : '[Lambda_b0 -> pi+ ^pi- (Lambda0 -> p+ pi-)]CC',
        'P'  : '[Lambda_b0 -> pi+ pi- (Lambda0 -> ^p+ pi-)]CC',
        'Pi' : '[Lambda_b0 -> pi+ pi- (Lambda0 -> p+ ^pi-)]CC',
}

# TupleTools
MyToolList = [
    "TupleToolKinematic"
    ,"TupleToolPid"
    ,"TupleToolTrackInfo"
    ,"TupleToolPrimaries"
    ,"TupleToolPropertime"
    ,"TupleToolEventInfo"
    ,"TupleToolTrackInfo"
    ,"TupleToolRecoStats"
    ,"TupleToolGeometry"
]

# Trigger list
triggerListL0 = [ "L0GlobalDecision"
                  ,"L0PhysDecision"
                  ,"L0HadronDecision"
                  ,"L0MuonDecision"
                  ,"L0MuonHighDecision"
                  ,"L0DiMuonDecision"
                  ,"L0PhotonDecision"
                  ,"L0ElectronDecision" ]
triggerListHlt1 = [ "Hlt1TrackMVADecision"
                    ,"Hlt1TrackMVALooseDecision"
                    ,"Hlt1TwoTrackMVADecision"
                    ,"Hlt1TwoTrackMVALooseDecision"
                    ,"Hlt1L0AnyDecision"
                    ,"Hlt1MBNoBiasDecision" ]
triggerListHlt2 = ["Hlt2Topo2BodyDecision",
                   "Hlt2Topo3BodyDecision",
                   "Hlt2Topo4BodyDecision",
                   "Hlt2Topo2BodyBBDTDecision",
                   "Hlt2Topo3BodyBBDTDecision",
                   "Hlt2Topo4BodyBBDTDecision"]
myTriggerList = triggerListL0 + triggerListHlt1 + triggerListHlt2

# LoKi
LoKi_Kine = LoKi__Hybrid__TupleTool("LoKi_Kine")
LoKi_Kine.Variables = {
   "MAXDOCA" : "DOCAMAX",
   "DOCA12"  : "DOCACHI2MAX",
   "Y"    : "Y",
   "ETA"  : "ETA",
}

# fill decay tree tuples
dtts = []
ii = 0
for name in lines:
    dtts.append( DecayTreeTuple(name) )
    dtts[ii].Inputs = ['AllStreams/Phys/{0}/Particles'.format(lines[name])]
    dtts[ii].Decay = decays
    dtts[ii].Branches = branches
    ii += 1

for dtt in dtts:
    # add decays
    dtt.addTool(TupleToolDecay, name='Lb')
    dtt.addTool(TupleToolDecay, name='L0')

    # true info
    dtt.addTupleTool("TupleToolMCTruth/TupleToolMCTruth")
    dtt.TupleToolMCTruth.ToolList = [
            "MCTupleToolKinematic"
            ,"MCTupleToolHierarchy"
            ,"MCTupleToolDecayType"
            ,"MCTupleToolReconstructed"
            ,"MCTupleToolPID"
    ]
    dtt.addTupleTool("TupleToolMCBackgroundInfo/TupleToolMCBackgroundInfo")

    # TISTOS
    dtt.Lb.ToolList += [ "TupleToolTISTOS" ]
    dtt.Lb.addTool(TupleToolTISTOS, name="TupleToolTISTOS" )
    dtt.Lb.TupleToolTISTOS.Verbose=True
    dtt.Lb.TupleToolTISTOS.VerboseHlt1=True
    dtt.Lb.TupleToolTISTOS.VerboseHlt2=True
    dtt.Lb.TupleToolTISTOS.TriggerList = myTriggerList
    dtt.L0.ToolList += [ "TupleToolTISTOS" ]
    dtt.L0.addTool(TupleToolTISTOS, name="TupleToolTISTOS" )
    dtt.L0.TupleToolTISTOS.Verbose=True
    dtt.L0.TupleToolTISTOS.VerboseHlt1=True
    dtt.L0.TupleToolTISTOS.VerboseHlt2=True
    dtt.L0.TupleToolTISTOS.TriggerList = myTriggerList

    # Decay Tree Fitter
    # KpKm
    subs = {
            'Lambda_b0 -> ^X+ X- (Lambda0 -> p+ pi-)' : 'K+',
            'Lambda_b0 -> X+ ^X- (Lambda0 -> p+ pi-)' : 'K-',
            'Lambda_b~0 -> ^X- X+ (Lambda~0 -> p~- pi+)' : 'K-',
            'Lambda_b~0 -> X- ^X+ (Lambda~0 -> p~- pi+)' : 'K+'
    }
    dtt.Lb.ToolList += ["TupleToolDecayTreeFitter/DTF_KpKm"]
    dtt.Lb.addTool(TupleToolDecayTreeFitter("DTF_KpKm"))
    dtt.Lb.DTF_KpKm.Verbose = True
    dtt.Lb.DTF_KpKm.UpdateDaughters = True
    dtt.Lb.DTF_KpKm.Substitutions = subs
    #dtt.Lb.DTF_KpKm.constrainToOriginVertex = True
    dtt.Lb.ToolList += ["TupleToolDecayTreeFitter/DTF_KpKm_PV"]
    dtt.Lb.addTool(TupleToolDecayTreeFitter("DTF_KpKm_PV"))
    dtt.Lb.DTF_KpKm_PV.Verbose = True
    dtt.Lb.DTF_KpKm_PV.UpdateDaughters = True
    dtt.Lb.DTF_KpKm_PV.Substitutions = subs
    dtt.Lb.DTF_KpKm_PV.constrainToOriginVertex = True
    # PipPim
    dtt.Lb.ToolList += ["TupleToolDecayTreeFitter/DTF_PipPim"]
    dtt.Lb.addTool(TupleToolDecayTreeFitter("DTF_PipPim"))
    dtt.Lb.DTF_PipPim.Verbose = True
    dtt.Lb.DTF_PipPim.UpdateDaughters = True
    #dtt.Lb.DTF_PipPim.constrainToOriginVertex = True
    dtt.Lb.ToolList += ["TupleToolDecayTreeFitter/DTF_PipPim_PV"]
    dtt.Lb.addTool(TupleToolDecayTreeFitter("DTF_PipPim_PV"))
    dtt.Lb.DTF_PipPim_PV.Verbose = True
    dtt.Lb.DTF_PipPim_PV.UpdateDaughters = True
    dtt.Lb.DTF_PipPim_PV.constrainToOriginVertex = True

	# LoKi Tool
    dtt.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Kine"]
    dtt.addTool(LoKi_Kine)

    # common tools
    dtt.ToolList += MyToolList


"""
# Momentum scale
from PhysConf.Selections import MomentumScaling, AutomaticData, TupleSelection
my_sels = []
for name in lines:
    my_sels.append( AutomaticData('Phys/{0}/Particles'.format(lines[name])) )
my_selection = [ MomentumScaling ( sel ) for sel in my_sels ]
from PhysConf.Selections import SelectionSequence
selseq = [ SelectionSequence('MomScale'+str(ii), my_selection[ii]).sequence() for ii in range(len(my_selection))]
"""

# MCDecayTree
mcdtt = MCDecayTreeTuple("mcntuple")
mcdtt.Decay = '[Lambda_b0 ==> ^K+ ^K- ^(Lambda0 ==> ^p+ ^pi-)]CC'
mcdtt.Branches = {
        'Lb' : '^([Lambda_b0 ==> K+ K- (Lambda0 ==> p+ pi-)]CC)',
        'L0' : '[Lambda_b0 ==> K+ K- ^(Lambda0 ==> p+ pi-)]CC',
        'hp' : '[Lambda_b0 ==> ^K+ K- (Lambda0 ==> p+ pi-)]CC',
        'hm' : '[Lambda_b0 ==> K+ ^K- (Lambda0 ==> p+ pi-)]CC',
        'P'  : '[Lambda_b0 ==> K+ K- (Lambda0 ==> ^p+ pi-)]CC',
        'Pi' : '[Lambda_b0 ==> K+ K- (Lambda0 ==> p+ ^pi-)]CC',
}


# DaVinci configuration
from Configurables import DaVinci
#from Configurables import CondDB
#CondDB ( LatestGlobalTagByDataType = year )
DaVinci().DDDBtag = "dddb-20170721-3"
DaVinci().CondDBtag = "sim-20170721-2-vc-md100"
DaVinci().UserAlgorithms += dtts + [ mcdtt ]
DaVinci().PrintFreq = 5000
DaVinci().SkipEvents = 0
DaVinci().Simulation = True
DaVinci().Lumi = not DaVinci().Simulation
DaVinci().EvtMax = -1
DaVinci().DataType = year
DaVinci().InputType = 'DST'
#DaVinci().RootInTES = '/Event/{0}'.format(stream)
#DaVinci().RootInTES = 'AllStreams'
#DaVinci().Turbo = True
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().TupleFile = 'Tuple.root'
# XML summary
from Configurables import LHCbApp
LHCbApp().XMLSummary="summary.xml"


# Local test only
TEST = False
if TEST:
    DaVinci().EvtMax = 500
    DaVinci().PrintFreq = 100
    from GaudiConf import IOHelper
    IOHelper().inputFiles([
      'root://door03.pic.es:1094/pnfs/pic.es/data/lhcb/MC/2016/ALLSTREAMS.DST/00069288/0000/00069288_00000037_7.AllStreams.dst',
      'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/2016/ALLSTREAMS.DST/00069288/0000/00069288_00000012_7.AllStreams.dst',
      'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2016/ALLSTREAMS.DST/00069288/0000/00069288_00000007_7.AllStreams.dst',
    ], clear=True)
